﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1Anagram
{
    public class Anagram
    {
        public string inputLine { get; set; }

        private string ReverseWord(string inputWord)
        {
            char firstSymbol, lastSymbol;
            char[] charWord = inputWord.ToCharArray();
            for (int i = 0, j = charWord.Length - 1; i < charWord.Length && j > i; i++, j--) //Заменил битовый & на логический &&
            {
                firstSymbol = charWord[i];
                lastSymbol = charWord[j];
                if (!char.IsLetter(charWord[j]) && !char.IsLetter(charWord[i]))
                {
                    continue;
                }
                if (!char.IsLetter(charWord[i]) && char.IsLetter(charWord[j]))
                {
                    firstSymbol = charWord[i + 1];
                    charWord[i + 1] = lastSymbol;
                    charWord[j] = firstSymbol;
                }
                else if (!char.IsLetter(charWord[j]) && char.IsLetter(charWord[i]))
                {
                    lastSymbol = charWord[j - 1];
                    charWord[i] = lastSymbol;
                    charWord[j - 1] = firstSymbol;
                }

                else
                {
                    charWord[i] = lastSymbol;
                    charWord[j] = firstSymbol;
                }
            }
            return new string(charWord); //return new string(charWord);
        }
        public string GetReversedString()
        {
            string inputString = this.inputLine;
            string[] words = inputString.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                words[i] = ReverseWord(words[i]);
            }
            string reversedString = string.Join(" ", words);
            Console.WriteLine(reversedString);
            return reversedString;
        }

        public void SetValue()
        {
            Console.WriteLine("Введите строку для  обработки ");
            string inputString = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(inputString))
            {
                Console.WriteLine("Невозможно обработать  пустую строку,введите заново");
                inputString = Console.ReadLine();
            }
            this.inputLine = inputString;
        }
    }
}
