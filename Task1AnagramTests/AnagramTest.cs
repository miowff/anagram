using NUnit.Framework;
using Task1Anagram;
using System;
namespace Task1AnagramTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestInputValue()
        {
            //arrange
            string testString = "a1bcd efg!h";
            string expextedString = "d1cba hgf!e";
            //act
            Anagram anagram = new Anagram();
            anagram.inputLine = testString;
            string actual = anagram.GetReversedString();
            //assert
            Assert.AreEqual(expextedString, actual);
        }
        [Test]
        public void TestInputValueOnlyNumbers()
        {
            //arrange
            string testString = "12345";
            string expextedString = "12345";
            //act
            Anagram anagram = new Anagram();
            anagram.inputLine = testString;
            string actual = anagram.GetReversedString();
            //assert
            Assert.AreEqual(expextedString, actual);
        }
        [Test]
        public void TestInputValueWord()
        {
            //arrange
            string testString = "Test";
            string expextedString = "tseT";
            //act
            Anagram anagram = new Anagram();
            anagram.inputLine = testString;
            string actual = anagram.GetReversedString();
            //assert
            Assert.AreEqual(expextedString, actual);
        }
        [Test]
        public void TestInputValueFiveWords()
        {
            //arrange
            string testString = "This is test of anagram";
            string expextedString = "sihT si tset fo margana";
            //act
            Anagram anagram = new Anagram();
            anagram.inputLine = testString;
            string actual = anagram.GetReversedString();
            //assert
            Assert.AreEqual(expextedString, actual);
        }
        [Test]
        public void TestInputValueFiveWordsPlusSymbols()
        {
            //arrange
            string testString = "Thi!s is t1est o2f an3agra!m";
            string expextedString = "sih!T si t1set f2o ma3rgna!a";
            //act
            Anagram anagram = new Anagram();
            anagram.inputLine = testString;
            string actual = anagram.GetReversedString();
            //assert
            Assert.AreEqual(expextedString, actual);
        }
    }
}